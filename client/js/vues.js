/* global state getQuizzes */

// //////////////////////////////////////////////////////////////////////////////
// HTML : fonctions génération de HTML à partir des données passées en paramètre
// //////////////////////////////////////////////////////////////////////////////
// génération d'une liste de quizzes avec deux boutons en bas
const htmlQuizzesList = (quizzes, curr, total) => {
  console.debug(`@htmlQuizzesList(.., ${curr}, ${total})`);

  // un élement <li></li> pour chaque quizz. Noter qu'on fixe une donnée
  // data-quizzid qui sera accessible en JS via element.dataset.quizzid.
  // On définit aussi .modal-trigger et data-target="id-modal-quizz-menu"
  // pour qu'une fenêtre modale soit affichée quand on clique dessus
  // VOIR https://materializecss.com/modals.html
  const quizzesLIst = quizzes.map(
    (q) =>
    `<li class="collection-item modal-trigger cyan lighten-5" data-target="id-modal-quizz-menu" data-quizzid="${q.quiz_id}">
    <h5>${q.title}</h5>
    ${q.description} <a class="chip">${q.owner_id}</a>
    </li>`
  );

  // le bouton "<" pour revenir à la page précédente, ou rien si c'est la première page
  // on fixe une donnée data-page pour savoir où aller via JS via element.dataset.page
  const prevBtn =
  curr !== 1
  ? `<button id="id-prev-quizzes" data-page="${curr -
    1}" class="btn"><i class="material-icons">navigate_before</i></button>`
    : '';

    // le bouton ">" pour aller à la page suivante, ou rien si c'est la première page
    const nextBtn =
    curr !== total
    ? `<button id="id-next-quizzes" data-page="${curr +
      1}" class="btn"><i class="material-icons">navigate_next</i></button>`
      : '';

      // La liste complète et les deux boutons en bas
      const html = `
      <ul class="collection">
      ${quizzesLIst.join('')}
      </ul>
      <div class="row">
      <div class="col s6 left-align">${prevBtn}</div>
      <div class="col s6 right-align">${nextBtn}</div>
      </div>
      `;
      return html;
};

// //////////////////////////////////////////////////////////////////////////////
// RENDUS : mise en place du HTML dans le DOM et association des événemets
// //////////////////////////////////////////////////////////////////////////////

console.debug(`@renderQuizzes()`);

// met la liste HTML dans le DOM et associe les handlers aux événements
// eslint-disable-next-line no-unused-vars

function renderQuizzes() {
  console.debug(`@renderQuizzes()`);
  // les éléments à mettre à jour : le conteneur pour la liste des quizz
  const usersElt = document.getElementById('id-all-quizzes-list');
  // une fenêtre modale définie dans le HTML
  //const modal = document.getElementById('id-modal-quizz-menu');

  // on appelle la fonction de génération et on met le HTML produit dans le DOM
  usersElt.innerHTML = htmlQuizzesList(
    state.quizzes.results,
    state.quizzes.currentPage,
    state.quizzes.nbPages
  );

  // /!\ il faut que l'affectation usersElt.innerHTML = ... ait eu lieu pour
  // /!\ que prevBtn, nextBtn et quizzes ne soient pas null
  // les éléments à mettre à jour : les boutons
  const prevBtn = document.getElementById('id-prev-quizzes');
  const nextBtn = document.getElementById('id-next-quizzes');
  // la liste de tous les quizzes individuels
  const quizzes = document.querySelectorAll('#id-all-quizzes-list li');

  // les handlers quand on clique sur "<" ou ">"
  function clickBtnPager() {
    // remet à jour les données de state en demandant la page
    // identifiée dans l'attribut data-page
    // noter ici le 'this' QUI FAIT AUTOMATIQUEMENT REFERENCE
    // A L'ELEMENT AUQUEL ON ATTACHE CE HANDLER
    getQuizzes(this.dataset.page);
  }
  if (prevBtn) prevBtn.onclick = clickBtnPager;
  if (nextBtn) nextBtn.onclick = clickBtnPager;

  // qd on clique sur un quizz, on change sont contenu avant affichage
  // l'affichage sera automatiquement déclenché par materializecss car on
  // a définit .modal-trigger et data-target="id-modal-quizz-menu" dans le HTML
  function clickQuiz() {
    const quizzId = this.dataset.quizzid;
    console.debug(`@clickQuiz(${quizzId})`);
    const addr = `${state.serverUrl}/quizzes/${quizzId}`;
    const html = `
    <p>Vous pourriez aller voir <a href="${addr}">${addr}</a>
    ou <a href="${addr}/questions">${addr}/questions</a> pour ses questions<p>.`;
    //modal.children[0].innerHTML = html;
    state.currentQuizz = quizzId;
    // eslint-disable-next-line no-use-before-define
    renderCurrentQuizz();
  }

  // pour chaque quizz, on lui associe son handler
  quizzes.forEach((q) => {
    q.onclick = clickQuiz;
  });
}

// affiche les quizz et permet d'y répondre
function renderCurrentQuizz() {
  const main = document.getElementById('id-all-quizzes-main'); //permet de récupérer l'endroit où on écrit les quizz
  main.innerHTML = ""; //vide le main
  const quiz_id = state.currentQuizz;
  const urlQuestion = `${state.serverUrl}/quizzes/${quiz_id}/questions`;
  const urlQuizz = `${state.serverUrl}/quizzes/${quiz_id}/`; //chemin pour le fetch

// fetch qui permet de récupérer les informations de chaque quizz et crée l'affichage
  fetch(urlQuizz, { method: 'GET', headers: state.headers() })
  .then(filterHttpResponse)
  .then((data) => {
    const user = document.createElement("p");
    const date = document.createElement("p");

    const titre = data.title + "<p>" + data.description + "<p>" + data.owner_id;
    user.setAttribute("style","font-size: 25px;");
    user.innerHTML = titre;
    date.innerHTML = data.created_at;

    main.appendChild(user);
    main.appendChild(date);

    state.ownerId = data.owner_id;

//test si la persnonne est connectée et si un quizz lui appartient
//si oui, crée les boutons pour modifier un quizz et ajouter / modifier / supprimer une question
    if(state.user && state.user.user_id == state.ownerId){
      const p1 = document.createElement("p");
      const bouton2 = document.createElement("button");
      bouton2.setAttribute("class", "btn waves-effect waves-light");
      bouton2.setAttribute("id", "modif-qcm");
      bouton2.setAttribute("onclick","renderModifQuestion()")

      bouton2.innerHTML = "Modifier QCM";
      p1.appendChild(bouton2);
      main.appendChild(p1);

      const p0 = document.createElement("p");
      const bouton = document.createElement("button");
      bouton.setAttribute("class", "btn waves-effect waves-light");
      bouton.setAttribute("id", "contrib-qcm");
      bouton.setAttribute("onclick","renderAjoutQuestion()")

      bouton.innerHTML = "Ajouter une question";
      p0.appendChild(bouton);
      main.appendChild(p0);
    }
  });

//permet de récupérer les questions et les propositions du QCM
  fetch(urlQuestion, { method: 'GET', headers: state.headers() })
  .then(filterHttpResponse)
  .then((data) => {
    data.forEach(x => {
      const questionNb = x.question_id+1;
      const titre = document.createElement("b");

      titre.setAttribute("style","font-size: 15px;");
      titre.setAttribute("id", questionNb);
      titre.innerHTML = " Q."+ questionNb +" : "+ x.sentence;

      //test si la persnonne est connectée et si un quizz lui appartient
      //si oui, crée les boutons pour ajouter / modifier / supprimer une question
      if(state.user &&  state.user.user_id == state.ownerId){

        const bouton3 = document.createElement("button");
        const supprimer = document.createElement("button");
        const id_question = "modifQuestion("+questionNb+")";
        const supprimer_Question = "supprimer("+questionNb+")";

        bouton3.setAttribute("id", "ModifierQuestion");
        bouton3.setAttribute("class", "btn waves-effect waves-light green");
        bouton3.setAttribute("onclick",id_question);

        supprimer.setAttribute("class", "btn waves-effect waves-light red");
        supprimer.setAttribute("onclick",supprimer_Question);

        bouton3.innerHTML = "...";
        supprimer.innerHTML = "X";

        main.appendChild(bouton3);
        main.appendChild(supprimer);
      }

      main.appendChild(titre);
      const formulaire = document.createElement("form");
      x.propositions.forEach(x => {
        if(x.content != ""){
          const p = document.createElement("p");
          const label = document.createElement("label");
          const span = document.createElement("span");
          const input = document.createElement("input");
          const id = x.proposition_id;
          const radioId = "radio"+ questionNb;
          const questionId = questionNb-1;
          const repondre = "repondre("+id+","+questionId+")";

          formulaire.setAttribute("action","#")
          input.setAttribute("type","radio");
          input.setAttribute("name",radioId);
          input.setAttribute("id",id);
          input.setAttribute("onclick",repondre);

          if(state.user){
            const urlAnswers = `${state.serverUrl}/users/answers`;
            //permet de savoir quelle case a été cochée et de préremplir le QCM avec les réponses données précédemment
            fetch(urlAnswers , {method: "get",headers: state.headers()})
            .then(filterHttpResponse)
            .then((data) => {
              data.forEach(x => {
                //condition pour savoir si le quizz sur lequel l'utilisateur-trice a cliqué fait parti des quizz auquel il ou elle a répondu
                if(state.user && x.quiz_id == quiz_id){
                  x.answers.forEach(y => {
                    const reponsenb = y.proposition_id;
                    //si l'utilisateur-trice a répondu a une question, ça "check" le bouton radio
                    if( y.proposition_id == id)
                    {
                      input.checked = true;
                    }
                  })
                }
              })
            });
          }
          span.innerHTML =  x.content;
          label.appendChild(input);
          label.appendChild(span);
          p.appendChild(label);
          formulaire.appendChild(p);
        }
      });
      //attribue un id à une question
      const idQuestion = "Q" + x.question_id;
      formulaire.setAttribute("id",idQuestion)
      //ajoute tout le formulaire au bloc "main"
      main.appendChild(formulaire);
    })
  })
}

//affiche la liste des QCM auquel un.e utilisateur-trice a répondu s'il ou elle est connecté.e
function renderMyAnswer() {
  console.debug(`@RenderMyQuizzes()`);
  //vérifie la connexion
  if(state.user){
    const answers = `${state.serverUrl}/users/answers`;
    //récupère la liste des qcm auxquels on a répondu
    fetch(answers, { method: 'GET', headers: state.headers() })
    .then(filterHttpResponse)
    .then((data) => {
      const MyQuizzesLIst = data.map(  //génère un tableau et affiche les informations des qcm (titre, description, createur-trice)
        x => `
        <li class="collection-item modal-trigger cyan lighten-5" data-target="id-modal-quizz-menu" data-quizzid="${x.quiz_id}">
        <h5>${x.title}</h5>
        ${x.description}
        <a class="chip">${x.owner_id}</a>
        </li>
        `
      );
      //permet d'écrire tout le contenu de "MyQuizzesLIst" sur notre écran
      document.getElementById('id-my-answers-list').innerHTML = `<ul class="collection">` + MyQuizzesLIst.join('') + `</ul>`;

      //récupère l'id du quizz et appelle renderMyCurrentAnswer
      function ClickMyQuiz() {
        const quizzId = this.dataset.quizzid;
        console.debug(`@clickQuiz(${quizzId})`);
        state.currentQuizz = quizzId;
        renderMyCurrentAnswer()
      }
      //permet de savoir sur quel quizz on a cliqué
      const my_quizzes_li=document.getElementById("id-my-answers-list").querySelectorAll("li");
      my_quizzes_li.forEach((q) => {
        q.onclick = ClickMyQuiz;
      });
    });
  }
  //cas où l'utilisateur-trice n'est pas connecté
  //affiche un message de gestion d'erreur
  else
  {
    const co = document.getElementById('id-my-answers-list')
    co.innerHTML = "<center><h5>Veuillez vous connecter pour voir vos réponses</h5></center>";
  }
}

//affiche les réponses données par l'utilisateur-trice à un quizz
function renderMyCurrentAnswer() {
  if(state.user){
    const  quiz_id = state.currentQuizz; //permet de récupérer le quizz sur lequel on est
    const urlAnswers = `${state.serverUrl}/users/answers`;
    const main = document.getElementById('id-my-answers-main'); //pour récupérer le corps de l'affichage
    main.innerHTML = ""; //et le vider
    //récupère le tableau de tous les quizz auquel on a répondu
    fetch(urlAnswers, { method: 'GET', headers: state.headers() })
    .then(filterHttpResponse)
    .then((data) => {
      data.forEach(x => {
        //test si le quizz sur lequel l'utilisateur-trice a cliqué est présent dans
        //la liste des quizz auxquels l'utilisateur-trice a répondu
        //si oui, génère l'affichage des réponses
        if(x.quiz_id == quiz_id){
          const titre = document.createElement("b");

          titre.setAttribute("style","font-size: 15px;");
          titre.innerHTML = x.description + " de " + x.owner_id;
          main.appendChild(titre);

          x.answers.forEach(y => {
            const question = document.createElement("p");
            const answer = document.createElement("p");
            const questionnb = y.question_id+1;
            const reponsenb = y.proposition_id+1;
            question.innerHTML = "Question : " + questionnb ;
            answer.innerHTML = "Rep : " + reponsenb;

            main.appendChild(question);
            main.appendChild(answer);
          })
        }
      })
    })
  }
  else{
    alert("Veuillez vous connecter pour répondre");
  }
}


// //////////////////////////////////////////////////////////////////////////////
// RENDUS : Affichage du formulaire html par du js
// //////////////////////////////////////////////////////////////////////////////

//crée le bouton pour créer un QCM
function btnAjout(){
  const qcm = document.getElementById('Qcm');
  qcm.innerHTML = "Créer un Qcm";
}

//fait disparaitre le bouton pour créer un QCM (suivant dans quel onglet on se trouve)
//vide le corps des QCM
function btnSuppr(){
  if(state.user){
    const qcm = document.getElementById('Qcm');
    qcm.innerHTML = "";
  }
  document.getElementById('id-all-quizzes-main').innerHTML = "";
  document.getElementById('id-my-answers-main').innerHTML = "";
}

//crée un bouton pour s'identifier si on n'est pas connecté
//le modifie en bouton de déconnexion si on est connecté
function renderUserBtn(){
  const btn = document.getElementById('login');
  btn.onclick = () =>
  {
    // on modifie l'état :
    if (state.user)  // si connectée
    {
      // quand on clique sur le nom de l'utilisateur-trice une boite de dialogue apparait pour confirmer la déconnection.
      const res = confirm(`Voulez-vous vous déconnecter ?`);
      if(res)// si confirm = true
      {
        state.xApiKey = undefined; // on change le xApiKey
        getUser()
        .then(data =>
          {
            btn.textContent = `S'identifier`;
            document.getElementById('id-my-quizzes-main').innerHTML = "";
            document.getElementById('id-my-answers-main').innerHTML = "";
            document.getElementById('id-all-quizzes-main').innerHTML = "";
            // une fois déconnecté le nom du user change en S'identifier
          })
        }
      }

      else // si déconnecté
      {
        state.xApiKey = prompt("Saisissez votre clef d'API");
        // quand on clique sur le bouton de login, on doit entrée notre clef API
        getUser()
        .then(data =>
          {
            if(state.user)
            {
              btn.textContent = `Bonjour ${state.user.firstname} ${state.user.lastname}`;
              btnAjout();
              // une fois indentifié, le bouton change et les nom et prenom du user sont affichés
            }
          })
        }
      }
}

//crée un formulaire pour ajouter un QCM
function renderFormulaireAjout(){
  //vérifie si l'utilisateur-trice est connecté
  if(state.user){
    const quiz_id = state.currentQuizz;
    const urlQuestion = `${state.serverUrl}/quizzes/${quiz_id}/questions`;
    const urlQuizz = `${state.serverUrl}/quizzes/${quiz_id}/`;
    const main = document.getElementById('id-all-quizzes-main');
    main.innerHTML = "";

    const formulaire = document.createElement("form");
    const p1 = document.createElement("div");
    const p3 = document.createElement("div");

    const inputTitre = document.createElement("input");
    const labelTitre = document.createElement("label");

    const inputDesc = document.createElement("input");
    const labelDesc = document.createElement("label");

    formulaire.setAttribute("class","col s12");
    p1.setAttribute("class","input-field col s6")

    //TITRE
    inputTitre.setAttribute("class","validate");
    inputTitre.setAttribute("id","name");
    inputTitre.setAttribute("type","text");
    labelTitre.setAttribute("for","name");

    labelTitre.innerHTML =  "Titre";

    p1.appendChild(inputTitre);
    p1.appendChild(labelTitre);
    formulaire.appendChild(p1);
    main.appendChild(formulaire);
    //FIN TITRE

    //DESCRIPTION
    inputDesc.setAttribute("class","input-field col s12");
    inputDesc.setAttribute("id","description");
    inputDesc.setAttribute("type","text");
    labelDesc.setAttribute("for","description");

    labelDesc.innerHTML =  "description";

    p3.setAttribute("class","input-field col s12")
    p3.appendChild(inputDesc);
    p3.appendChild(labelDesc);
    formulaire.appendChild(p3);
    main.appendChild(formulaire);
    //FIN DESCRIPTION

    //BOUTON
    const bouton = document.createElement("button");
    bouton.setAttribute("class", "btn waves-effect waves-light");
    bouton.setAttribute("onclick", "ajout()");

    bouton.innerHTML = "Créer";
    main.appendChild(bouton);
    //FIN BOUTON
  }
  else{
    alert("Veuillez vous connecter");
  }
}

//Pour ajouter une question à un QCM
function renderAjoutQuestion(){
  //si on est connecté
  if(state.user){
    const quiz_id = state.currentQuizz;
    const urlQuestion = `${state.serverUrl}/quizzes/${quiz_id}/questions`;
    const urlQuizz = `${state.serverUrl}/quizzes/${quiz_id}/`;
    const main = document.getElementById('id-all-quizzes-main');
    main.innerHTML = "";

    const formulaire = document.createElement("form");
    const p1 = document.createElement("p");
    const p2 = document.createElement("p");
    const p3 = document.createElement("p");
    const p4 = document.createElement("p");

    const inputQuestion = document.createElement("input");
    const labelQuestion = document.createElement("label");
  //filtre pour ne garder que le dernier QCM
    const inputReponse1 = document.createElement("input");
    const labelReponse1 = document.createElement("label");

    const inputReponse2 = document.createElement("input");
    const labelReponse2 = document.createElement("label");

    const inputReponse3 = document.createElement("input");
    const labelReponse3 = document.createElement("label");

    formulaire.setAttribute("class","col s6");
    p1.setAttribute("class","input-field col s12")
    p2.setAttribute("class","input-field col s12")
    p3.setAttribute("class","input-field col s12")
    p4.setAttribute("class","input-field col s12")

    //crée le formulaire pour créer une nouvelle question dans un QCM
    //QUESTION
    inputQuestion.setAttribute("class","validate");
    inputQuestion.setAttribute("id","question");
    inputQuestion.setAttribute("type","text");

    labelQuestion.innerHTML =  "Question";

    p1.appendChild(inputQuestion);
    p1.appendChild(labelQuestion);
    formulaire.appendChild(p1);
    main.appendChild(formulaire);
    //FIN QUESTION

    //REPONSES
    ///////////////////////////proposition 1
    inputReponse1.setAttribute("class","validate");
    inputReponse1.setAttribute("id","reponse1");
    inputReponse1.setAttribute("type","text");

    labelReponse1.innerHTML =  "Reponse";

    p2.appendChild(inputReponse1);
    p2.appendChild(labelReponse1);getUser
    formulaire.appendChild(p2);
    main.appendChild(formulaire);

    ///////////////////////////proposition 2
    inputReponse2.setAttribute("class","validate");
    inputReponse2.setAttribute("id","reponse2");
    inputReponse2.setAttribute("type","text");

    labelReponse2.innerHTML =  "Reponse";

    p3.appendChild(inputReponse2);
    p3.appendChild(labelReponse2);
    formulaire.appendChild(p3);
    main.appendChild(formulaire);

    ////////////////////////////////proposition 3
    inputReponse3.setAttribute("class","validate");
    inputReponse3.setAttribute("id","reponse3");
    inputReponse3.setAttribute("type","text");

    labelReponse3.innerHTML =  "Reponse";

    p4.appendChild(inputReponse3);
    p4.appendChild(labelReponse3);
    formulaire.appendChild(p4);
    main.appendChild(formulaire);
    //FIN REPONSES

    //BOUTON
    const p0 = document.createElement("p");
    const bouton = document.createElement("button");
    bouton.setAttribute("class", "btn waves-effect waves-light");
    bouton.setAttribute("id", "ajout-Qcm");
    bouton.setAttribute("onclick","ajoutQuestion()");

    bouton.innerHTML = "Ajout question";

    p0.appendChild(bouton);
    formulaire.appendChild(p0);
    main.appendChild(formulaire);
    //FIN BOUTON
    }
  else{
    alert("Veuillez vous connecter");
  }
}

//crée un formulaire pré-rempli qui permet de modifier le titre et la description d'un QCM
function renderModifQuestion(){
  if(state.user){
    const quiz_id = state.currentQuizz;
    const urlQuizz = `${state.serverUrl}/quizzes/${quiz_id}/`;
    const main = document.getElementById('id-all-quizzes-main');
    main.innerHTML = "";

    const formulaire = document.createElement("form");
    const p1 = document.createElement("div");
    const p3 = document.createElement("div");

    const inputTitre = document.createElement("input");
    const labelTitre = document.createElement("label");

    const inputDesc = document.createElement("input");
    const labelDesc = document.createElement("label");

    formulaire.setAttribute("class","col s12");
    p1.setAttribute("class","input-field col s6")

    //TITRE
    inputTitre.setAttribute("class","validate");
    inputTitre.setAttribute("id","name");
    inputTitre.setAttribute("type","text");

    labelTitre.setAttribute("for","name");
    //FIN TITRE

    //DESCRIPTION
    inputDesc.setAttribute("class","input-field col s12");
    inputDesc.setAttribute("id","description");
    inputDesc.setAttribute("type","text");
    labelDesc.setAttribute("for","description");
    //FIN DESCRIPTION

    //récupère les informations du QCM et pré-rempli les champs
    fetch(urlQuizz, {method: "get",headers: state.headers()})
    .then(filterHttpResponse)
    .then((data) => {
      const title = data.title;
      const desc = data.description;
      inputTitre.setAttribute("value", title);
      inputDesc.setAttribute("value", desc);
    })
    p1.appendChild(inputTitre);
    p1.appendChild(labelTitre);
    p3.setAttribute("class","input-field col s12")
    p3.appendChild(inputDesc);
    p3.appendChild(labelDesc);
    formulaire.appendChild(p1);
    formulaire.appendChild(p3);
    main.appendChild(formulaire);

    //BOUTON
    const bouton = document.createElement("button");
    bouton.setAttribute("class", "btn waves-effect waves-light");
    bouton.setAttribute("onclick", "modifier()");

    bouton.innerHTML = "Modifier";
    main.appendChild(bouton);
    //FIN BOUTON
  }
  else{
    alert("Veuillez vous connecter");
  }
}

//crée un formulaire pré-rempli pour modiier une question et/ou ses propositions
// la variable id correspond à l'id d'une question
function modifQuestion(id){
  const indice = id-1;
  // Vide le formulaire
  const sup = "Q"+indice;
  const suppr = document.getElementById(sup);
  suppr.innerHTML = "";

  // FORMULAIRE
  const quiz_id = state.currentQuizz;
  const urlQuestion = `${state.serverUrl}/quizzes/${quiz_id}/questions`;
  const idquest = document.getElementById(sup);
  const p = document.createElement("p");
  const p1 = document.createElement("div");

  const inputTitre = document.createElement("input");
  const inputReponse1 = document.createElement("input");
  const inputReponse2 = document.createElement("input");
  const inputReponse3 = document.createElement("input");

  const id_input = "name"+id;
  const rep1 = "rep1"+id;
  const rep2 = "rep2"+id;
  const rep3 = "rep3"+id;

  p1.setAttribute("class","input-field col s6")

  //TITRE
  inputTitre.setAttribute("class","validate");
  inputTitre.setAttribute("id",id_input);
  inputTitre.setAttribute("type","text");
  //FIN TITRE

  //REPONSE
  inputReponse1.setAttribute("type","text");
  inputReponse2.setAttribute("type","text");
  inputReponse3.setAttribute("type","text");

  inputReponse1.setAttribute("id",rep1);
  inputReponse2.setAttribute("id",rep2);
  inputReponse3.setAttribute("id",rep3);
  //FIN REPONSE
  //récupère la question et les propositions de réponses, et pré-rempli les champs input
  fetch(urlQuestion, {method: "get",headers: state.headers()})
  .then(filterHttpResponse)
  .then((data) => {

    const sentence = data[indice].sentence;
    inputTitre.setAttribute("value",sentence);

    const reponse1 = data[indice].propositions[0].content;
    const reponse2 = data[indice].propositions[1].content;
    const reponse3 = data[indice].propositions[2].content;
    inputReponse1.setAttribute("value", reponse1);
    inputReponse2.setAttribute("value", reponse2);
    inputReponse3.setAttribute("value", reponse3);
  })

  p1.appendChild(inputTitre);
  p1.appendChild(inputReponse1);
  p1.appendChild(inputReponse2);
  p1.appendChild(inputReponse3);
  idquest.appendChild(p1);

  //BOUTON
  const modifSentence = "modifSentence("+id+")";
  const bouton = document.createElement("button");
  bouton.setAttribute("class", "btn waves-effect waves-light");
  bouton.setAttribute("id", "submitModifierQuestion");
  bouton.setAttribute("onclick",modifSentence);

  bouton.innerHTML = "Envoyer";
  p1.appendChild(bouton);
  p1.appendChild(p);
  idquest.appendChild(p1);
  //FIN BOUTON
  //FIN FORMULAIRE
}


// //////////////////////////////////////////////////////////////////////////////
// RENDUS : Fetch POST, PUT et DELETE
// //////////////////////////////////////////////////////////////////////////////

//récupère les réponses à un QCM et les envoies au server
//variable idRep = id d'une proposition de réponse
//variable questionId = id de la question
function repondre(idRep, questionId){
  if(state.user){
    const main = document.getElementById('id-all-quizzes-main');
    const quiz_id = state.currentQuizz;
    const urlQuestion = `${state.serverUrl}/quizzes/${quiz_id}/questions`;
    //permet de récupérer les informations de la question à laquelle on veut répondre
    fetch(urlQuestion, {method: "get",headers: state.headers()})
    .then(filterHttpResponse)
    .then((data) => {
      const propositions_id = idRep;
      const question_id = questionId;
      const rep = question_id+1;

      const urlAnswers = `${state.serverUrl}/quizzes/${quiz_id}/questions/${question_id}/answers/${propositions_id}`;
      //permet d'envoyer la réponse au server
      fetch(urlAnswers, {
        method: "post",
        headers: state.headers(),
        body: JSON.stringify({
          user_id: state.user.user_id,
          quiz_id: quiz_id,
          question_id: question_id,
          proposition_id: idRep,
          answered_at: ""
        })})
        document.getElementById(rep).innerHTML = "Enregistrée";
        //permet de rafraichir les données
        setTimeout(()=>{renderQuizzes();}, 1500);
        setTimeout(()=>{renderMyAnswer();}, 1500);
      });
    }
    else{
      alert("Veuillez vous connecter pour répondre à un QCM");
    }
  }

//Crée un QCM
function ajout(){
  const main = document.getElementById('id-all-quizzes-main');
  const urlajout = `${state.serverUrl}/quizzes/`;
  const titre = document.getElementById('name');
  const description = document.getElementById('description');
  //envoie les informations (titre + description) au server
  fetch(urlajout, {
    method: "post",
    headers: state.headers(),
    body: JSON.stringify({
      title: titre.value,
      description: description.value,
      open:"true"
    })
  })
  main.innerHTML = "Réponse enregistrée";
  setTimeout(()=>{qcmRefresh();}, 1500); //lance la fonction pour afficher le qcm créé
}

//affiche le qcm que l'on vient de créé
function qcmRefresh(){
  const quizz = `${state.serverUrl}/users/quizzes/`;
  //récupère tous les qcm
  //filtre pour ne garder que le dernier QCM
  //garde l'id du dernier quizz
  fetch(quizz, {method: "get",headers: state.headers()})
  .then(filterHttpResponse)
  .then((data) => {
    console.log(data);
    const last = data.length-1;
    state.currentQuizz = data[last].quiz_id;
  })
  setTimeout(()=>{renderCurrentQuizz();}, 1500);//affiche le QCM
}

//Permet d'ajouter une question à un quizz
function ajoutQuestion(){
  const quiz_id = state.currentQuizz;
  const urlQuestion = `${state.serverUrl}/quizzes/${quiz_id}/questions`;
  const main = document.getElementById('id-all-quizzes-main');
  const urlajout = `${state.serverUrl}/quizzes/`;
  const question = document.getElementById('question');
  const reponse1 = document.getElementById('reponse1');
  const reponse2 = document.getElementById('reponse2');
  const reponse3 = document.getElementById('reponse3');

  main.innerHTML = "";
  //récupère la question sur laquelle on est
  fetch(urlQuestion, {method: "get",headers: state.headers()})
  .then(filterHttpResponse)
  .then((data) => {
    const question_id = data.length;

    const urlAnswers = `${state.serverUrl}/quizzes/${quiz_id}/questions/`;

    //envoie les propositions au server (crée la question)
    fetch(urlAnswers, {
      method: "post",
      headers: state.headers(),
      body: JSON.stringify({
        quiz_id : quiz_id,
        question_id : question_id,
        sentence : question.value,
        propositions:
        [
          { content:reponse1.value, proposition_id:0, correct:"true"},
          { content:reponse2.value, proposition_id:1, correct:"false"},
          { content:reponse3.value, proposition_id:2, correct:"false"}
        ]
      })})
      main.innerHTML = "ok";
    });
    setTimeout(()=>{renderCurrentQuizz();}, 1500); //refresh sur le quizz actuel
  }

//Modifier le titre et la description d'un QCM
function modifier(){
  const main = document.getElementById('id-all-quizzes-main');
  const quiz_id = state.currentQuizz;
  const urlmodif = `${state.serverUrl}/quizzes/${quiz_id}/`;
  const titre = document.getElementById('name');
  const description = document.getElementById('description');
  //remplace les informations par le nouveau titre et la nouvelle description qu'on lui donne
  fetch(urlmodif, {
    method: "put",
    headers: state.headers(),
    body: JSON.stringify({
      title: titre.value,
      description: description.value,
      open:"true"
    })})
    main.innerHTML = "Réponse enregistrée";
    renderQuizzes();
    setTimeout(()=>{renderCurrentQuizz();}, 1500); //réaffiche le quizz sur lequel on est
}

//Modifier une question et ses propositions de réponses
//variable id = id de la quesion sur laquelle on est
function modifSentence(id){
  const quiz_id = state.currentQuizz;
  const question_id = id-1;
  const urlQuestion = `${state.serverUrl}/quizzes/${quiz_id}/questions/${question_id}`;
  const inputid = "name"+id;

  const inp1 = "rep1"+id;
  const inp2 = "rep2"+id;
  const inp3 = "rep3"+id;

  const question = document.getElementById(id);
  const input = document.getElementById(inputid);
  const rep1 = document.getElementById(inp1);
  const rep2 = document.getElementById(inp2);
  const rep3 = document.getElementById(inp3);
  //remplace les anciennes entrées (question + propositions) par les nouvelles
  fetch(urlQuestion, {
    method: "put",
    headers: state.headers(),
    body: JSON.stringify({
      sentence: input.value,
      propositions: [
        {
          content: rep1.value,
          proposition_id: 0,
          correct: true
        },
        {
          content: rep2.value,
          proposition_id: 1,
          correct: false
        },
        {
          content: rep3.value,
          proposition_id: 2,
          correct: false
        }
      ]
    })
  })
    question.innerHTML = "enregistrée";
    setTimeout(()=>{renderCurrentQuizz();}, 1500);
}

//Supprimer une question et ses propositions
//variable id = id de la quesion sur laquelle on est
function supprimer(id){
  const quiz_id = state.currentQuizz;
  const question_id = id-1;
  const urlQuestion = `${state.serverUrl}/quizzes/${quiz_id}/questions/${question_id}`;
  const inputid = "name"+id;
  const question = document.getElementById(id);
  //supprime une question en fonction de son id
  fetch(urlQuestion, {
    method: "delete",
    headers: state.headers(),
    body: JSON.stringify({
      quiz_id : quiz_id,
      question_id : question_id,
    })})
    question.innerHTML = "supprimé";
    //refresh le quizz
    setTimeout(()=>{renderCurrentQuizz();}, 1500);
    setTimeout(()=>{renderMyAnswer();}, 1500);
}
